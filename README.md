# Hacker News Articles

## Requirements

[Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/) installed

## Usage

Clone the repository or download the source code.

Create an `.env.production` file in the root folder with the following variables:

```
JWT_SECRET=
#DATABASE_HOST=
#DATABASE_PORT=
#SEED_ON_STARTUP=
#FETCH_ARTICLES_INTERVAL=
#PORT=
#ARTICLES_PER_PAGE=
```

**`JWT_SECRET`**: Secret used to sign JWTs. Required.

**`DATABASE_HOST`**: IP address of the database host. Optional. By default will attempt to connect to the database instance specified in the docker-compose file.

**`DATABASE_PORT`**: Database Port. Optional. Defaults to 27017.

**`SEED_ON_STARTUP`**: Wether to fetch articles on server startup. Treats `true` as truthy and anything else as falsy.

**`FETCH_ARTICLES_INTERVAL`**: Time in minutes in which the server will attempt to fetch articles from the hacker news endpoint. Defaults to 60 minutes.

**`PORT`**: Port in which the server will listen for incoming connections. Defaults to 3000.

**`ARTICLES_PER_PAGE`**: Maximum number of articles returned when querying all the articles. Defaults to 5.


### start

To start the API, open a terminal in the root folder of the project and run: `npm start`

### stop

To stop the client, open a terminal in the root folder of the project and run: `npm stop`

### Getting the IP Address of the API

To get the IP address, open a terminal in the root folder of the project and run: `npm run docker:get-ip`. This will list the containers started by docker-compose. The output will look like the following:

```
/reign-challenge_api_1 - 172.18.0.3
/reign-challenge_mongodb_1 - 172.18.0.2
```
