import { ApiProperty } from '@nestjs/swagger';

export * from './interfaces';

export enum MILLISECONDS {
  SECOND = 1000,
  MINUTE = SECOND * 60,
}

export class ErrorResponseDto {
  @ApiProperty()
  statusCode: number;

  @ApiProperty()
  message: string;
}
