import { ApiProperty } from '@nestjs/swagger';
import { Article, ArticleDocument } from '../article.schema';

export class GetArticlesResponseDto {
  @ApiProperty({ type: [Article] })
  data: ArticleDocument[];

  @ApiProperty()
  pageCount: number;

  @ApiProperty()
  currentPage: number;
}
