import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsNumber, IsString, Min, IsInt } from 'class-validator';
import { Transform } from 'class-transformer';

export class GetArticlesFilterDto {
  @Transform(({ value }) => +value)
  @ApiProperty({
    description: 'The page number',
    minimum: 0,
    example: 3,
    required: false,
  })
  @IsOptional()
  @IsNumber()
  @Min(0)
  @IsInt()
  page?: number;

  @ApiProperty({
    description: 'The search query',
    example: 'typescript',
    required: false,
  })
  @IsOptional()
  @IsString()
  search?: string | null;
}
