import { ApiProperty } from '@nestjs/swagger';
import { IsMongoId } from 'class-validator';

export class IdParamDto {
  @ApiProperty({
    description: 'Article ID',
    example: '61bd27c1cf34021b7d5b660f',
  })
  @IsMongoId({ message: 'The ID is not valid' })
  id: string;
}
