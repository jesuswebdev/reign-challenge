import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { Article, ArticleDocument } from './article.schema';
import { GetArticlesFilterDto } from './dto/get-articles-filter-dto';
import { CreateArticleDto } from './dto/create-article.dto';
import { ConfigService } from '@nestjs/config';
import { GetArticlesResponseDto } from './dto/get-articles-response.dto';

@Injectable()
export class ArticleService {
  constructor(
    @InjectModel(Article.name) private articleModel: Model<ArticleDocument>,
    private configService: ConfigService,
  ) {}

  async getArticles(
    getArticlesFilterDto: GetArticlesFilterDto,
  ): Promise<GetArticlesResponseDto> {
    const { page = 1, search } = getArticlesFilterDto;
    const monthIndex = [
      ,
      'january',
      'february',
      'march',
      'april',
      'may',
      'june',
      'july',
      'august',
      'september',
      'october',
      'november',
      'december',
    ].findIndex((month) => month === search?.toLowerCase());

    const limit = this.configService.get<number>('ARTICLES_PER_PAGE');
    const offset = +page > 1 ? +page * limit - limit : 0;

    const searchQuery = { $regex: new RegExp(search), $options: 'i' };

    const query = [
      { title: searchQuery },
      { story_title: searchQuery },
      { author: searchQuery },
      { _tags: { $elemMatch: searchQuery } },
      ...(monthIndex
        ? [{ $expr: { $eq: [{ $month: '$created_at' }, monthIndex] } }]
        : []),
    ];

    const count = await this.articleModel.countDocuments({ $or: query });
    const found = await this.articleModel.aggregate([
      { $match: { $or: query } },
      ...(offset ? [{ $skip: offset }] : []),
      ...(limit ? [{ $limit: limit }] : []),
    ]);
    return {
      data: found,
      currentPage: +page,
      pageCount: Math.ceil(count / limit),
    };
  }

  async getArticleById(id: string): Promise<ArticleDocument> {
    const found = await this.articleModel.findOne({
      _id: new Types.ObjectId(id),
    });

    if (!found) {
      throw new NotFoundException(`Article with ID '${id}' does not exist.`);
    }

    return found;
  }

  async deleteArticleById(id: string): Promise<void> {
    const result = await this.articleModel.findByIdAndDelete(id);

    if (!result) {
      throw new NotFoundException(`Article with ID '${id}' does not exist.`);
    }
  }

  async insertArticles(
    articles: CreateArticleDto[],
  ): Promise<ArticleDocument[]> {
    return this.articleModel.insertMany(articles);
  }
}
