import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @ApiProperty()
  _id: string;

  @Prop()
  @ApiProperty()
  title: string;

  @Prop()
  @ApiProperty()
  url: string;

  @Prop()
  @ApiProperty()
  author: string;

  @Prop()
  @ApiProperty()
  points: number;

  @Prop()
  @ApiProperty()
  story_text: string;

  @Prop()
  @ApiProperty()
  comment_text: string;

  @Prop()
  @ApiProperty()
  num_comments: number;

  @Prop()
  @ApiProperty()
  story_id: number;

  @Prop()
  @ApiProperty()
  story_title: string;

  @Prop()
  @ApiProperty()
  story_url: string;

  @Prop()
  @ApiProperty()
  parent_id: number;

  @Prop()
  @ApiProperty()
  created_at_i: number;

  @Prop()
  @ApiProperty()
  created_at: Date;

  @Prop([String])
  _tags: string[];

  @Prop({ unique: true })
  objectID: string;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
