import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { ArticleService } from './article.service';
import { getModelToken } from '@nestjs/mongoose';
import { NotFoundException } from '@nestjs/common';

interface MockedModel {
  countDocuments: jest.Mock;
  aggregate: jest.Mock;
  findOne: jest.Mock;
  findByIdAndDelete: jest.Mock;
  insertMany: jest.Mock;
}

const mockArticleModel = (): MockedModel => ({
  countDocuments: jest.fn(),
  aggregate: jest.fn(),
  findOne: jest.fn(),
  findByIdAndDelete: jest.fn(),
  insertMany: jest.fn(),
});

const mockConfigService = () => ({
  get: jest.fn(),
});

describe('ArticleService', () => {
  let service: ArticleService;
  let articleModel: MockedModel;
  let configService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticleService,
        { provide: getModelToken('Article'), useFactory: mockArticleModel },
        { provide: ConfigService, useFactory: mockConfigService },
      ],
    }).compile();

    service = module.get<ArticleService>(ArticleService);
    configService = module.get<ConfigService>(ConfigService);
    articleModel = module.get<MockedModel>(getModelToken('Article'));
  });

  describe('Get Articles', () => {
    it('calls the articleService.getArticles and returns the result', async () => {
      const resolvedValue = {
        currentPage: 3,
        data: [],
        pageCount: 5,
      };
      articleModel.aggregate.mockResolvedValue([]);
      articleModel.countDocuments.mockResolvedValue(25);
      (configService.get as jest.Mock).mockReturnValue(5);
      const result = await service.getArticles({
        page: 3,
        search: 'hello world',
      });
      expect(result).toEqual(resolvedValue);
    });
  });

  describe('Get Articles By ID', () => {
    it('calls the articleService.getArticleById and returns the result', async () => {
      articleModel.findOne.mockResolvedValue({});
      const result = await service.getArticleById('61bd27c1cf34021b7d5b6610');
      expect(result).toEqual({});
    });

    it('calls the articleService.getArticleById and throws a NotFoundException', async () => {
      articleModel.findOne.mockResolvedValue(null);
      expect(
        service.getArticleById('61bd27c1cf34021b7d5b6610'),
      ).rejects.toThrow(NotFoundException);
    });
  });

  describe('Delete Article', () => {
    it('calls the articleService.deleteArticleById and returns nothing', async () => {
      articleModel.findByIdAndDelete.mockResolvedValue({});
      const result = await service.deleteArticleById(
        '61bd27c1cf34021b7d5b6610',
      );
      expect(result).toEqual(undefined);
    });

    it('calls the articleService.deleteArticleById and throws a NotFoundException', async () => {
      articleModel.findByIdAndDelete.mockResolvedValue(null);
      expect(
        service.deleteArticleById('61bd27c1cf34021b7d5b6610'),
      ).rejects.toThrow(NotFoundException);
    });
  });

  describe('Insert Articles', () => {
    it('calls the articleService.insertArticles and returns the articles', async () => {
      const mockArticle = {
        created_at: '2021-12-20T17:03:36.000Z',
        title: null,
        url: null,
        author: 'mpweiher',
        points: null,
        story_text: null,
        comment_text: 'comment',
        num_comments: null,
        story_id: 29623319,
        story_title: 'Why Architecture Oriented Programming Matters (2019)',
        story_url:
          'https://blog.metaobject.com/2019/02/why-architecture-oriented-programming.html',
        parent_id: 29626827,
        created_at_i: 1640019816,
        _tags: ['comment', 'author_mpweiher', 'story_29623319'],
        objectID: '29627085',
      };

      articleModel.insertMany.mockResolvedValue([mockArticle]);
      const result = await service.insertArticles([mockArticle]);
      expect(result).toEqual([mockArticle]);
    });
  });
});
