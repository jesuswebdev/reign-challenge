import {
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ArticleService } from './article.service';
import { Article, ArticleDocument } from './article.schema';
import { GetArticlesFilterDto } from './dto/get-articles-filter-dto';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiInternalServerErrorResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { IdParamDto } from './dto/id-param.dto';
import { GetArticlesResponseDto } from './dto/get-articles-response.dto';
import { ErrorResponseDto } from '../utils';

@Controller('articles')
@ApiTags('articles')
@UseGuards(AuthGuard())
export class ArticleController {
  constructor(private articleService: ArticleService) {}

  @Get()
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get articles',
    description: 'Accepts query parameters for filtering and pagination',
  })
  @ApiOkResponse({ type: GetArticlesResponseDto })
  @ApiResponse({
    status: 401,
    description: 'Unauthorized request',
    type: ErrorResponseDto,
  })
  @ApiBadRequestResponse({
    description: 'Invalid query parameters',
    type: ErrorResponseDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: ErrorResponseDto,
  })
  getArticles(
    @Query() getArticlesFilterDto: GetArticlesFilterDto,
  ): Promise<GetArticlesResponseDto> {
    return this.articleService.getArticles(getArticlesFilterDto);
  }

  @Get('/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Get article by ID' })
  @ApiOkResponse({ type: Article, description: 'Ok response' })
  @ApiNotFoundResponse({
    description: 'Article with the given ID does not exist',
    type: ErrorResponseDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized request',
    type: ErrorResponseDto,
  })
  @ApiBadRequestResponse({
    description: 'Malformed article ID',
    type: ErrorResponseDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: ErrorResponseDto,
  })
  getArticleById(@Param() idParamDto: IdParamDto): Promise<ArticleDocument> {
    return this.articleService.getArticleById(idParamDto.id);
  }

  @HttpCode(204)
  @Delete('/:id')
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Delete article' })
  @ApiNoContentResponse({ description: 'Article was deleted' })
  @ApiBadRequestResponse({
    description: 'Malformed article ID',
    type: ErrorResponseDto,
  })
  @ApiNotFoundResponse({
    description: 'Article with the given ID does not exist',
    type: ErrorResponseDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Unauthorized request',
    type: ErrorResponseDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: ErrorResponseDto,
  })
  deleteArticleById(@Param() idParamDto: IdParamDto): Promise<void> {
    return this.articleService.deleteArticleById(idParamDto.id);
  }
}
