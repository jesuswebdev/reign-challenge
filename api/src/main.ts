import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ArticleService } from './article/article.service';
import axios from 'axios';
import { ConfigService } from '@nestjs/config';
import { MILLISECONDS } from './utils';

const initInterval = (
  articleService: ArticleService,
  configService: ConfigService,
) => {
  const callback = async () => {
    const { data } = await axios.get(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );

    await articleService.insertArticles(data.hits);
  };

  const seedOnStartup = configService.get<string>('SEED_ON_STARTUP');
  const fetchArticlesInterval = configService.get<number>(
    'FETCH_ARTICLES_INTERVAL',
  );

  if (seedOnStartup) {
    setTimeout(() => callback(), MILLISECONDS.SECOND * 10);
  }

  setInterval(callback, MILLISECONDS.MINUTE * fetchArticlesInterval);
};

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const articleService = app.get(ArticleService);
  const configService = app.get(ConfigService);

  const config = new DocumentBuilder()
    .addBearerAuth()
    .setTitle('Hacker News Articles')
    .setDescription('Description')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/api/docs', app, document);
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(configService.get<number>('PORT'));

  if (configService.get('NODE_ENV') !== 'test') {
    initInterval(articleService, configService);
  }
}
bootstrap();
