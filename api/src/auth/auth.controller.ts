import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import {
  ApiConflictResponse,
  ApiInternalServerErrorResponse,
  ApiNoContentResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { ErrorResponseDto } from '../utils';
import { AuthService } from './auth.service';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { SignInResponseDto } from './dto/signin-response.dto';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/signup')
  @HttpCode(204)
  @ApiOperation({
    summary: 'Sign up',
    description: 'Creates a user in the database',
  })
  @ApiNoContentResponse({ description: 'Signed up' })
  @ApiConflictResponse({
    description: 'Trying to sign up with an username that already exists',
    type: ErrorResponseDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: ErrorResponseDto,
  })
  signUp(@Body() authCredentialsDto: AuthCredentialsDto): Promise<void> {
    return this.authService.signUp(authCredentialsDto);
  }

  @Post('/signin')
  @HttpCode(200)
  @ApiOperation({
    summary: 'Signs in',
    description:
      'Provides the user with an access token after successfully signing in',
  })
  @ApiOkResponse({ type: SignInResponseDto })
  @ApiUnauthorizedResponse({
    description:
      'Given auth credentials do not match with any stored credentials',
    type: ErrorResponseDto,
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
    type: ErrorResponseDto,
  })
  signIn(
    @Body() authCredentialsDto: AuthCredentialsDto,
  ): Promise<SignInResponseDto> {
    return this.authService.signIn(authCredentialsDto);
  }
}
