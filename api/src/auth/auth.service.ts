import {
  ConflictException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { User, UserDocument } from './user.schema';
import { MongoError } from '../utils';
import { Password } from '../utils/Password';
import { JwtService } from '@nestjs/jwt';
import { SignInResponseDto } from './dto/signin-response.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    private jwtService: JwtService,
  ) {}

  async signUp(authCredentialsDto: AuthCredentialsDto): Promise<void> {
    const { username, password } = authCredentialsDto;
    try {
      await this.userModel.create({ username, password });
    } catch (error: unknown) {
      if ((error as MongoError)?.code === 11000) {
        throw new ConflictException('Username already exists');
      }
      throw error;
    }
  }

  async signIn(
    authCredentialsDto: AuthCredentialsDto,
  ): Promise<SignInResponseDto> {
    const { username, password } = authCredentialsDto;

    const user = await this.userModel.findOne({ username });

    if (!(await Password.compare(user?.password, password))) {
      throw new UnauthorizedException(
        'Combination of username/password not valid',
      );
    }

    const accessToken = await this.jwtService.signAsync({
      username: user.username,
    });

    return { accessToken };
  }
}
