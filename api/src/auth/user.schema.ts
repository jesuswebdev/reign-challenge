import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Password } from '../utils/Password';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({ unique: true })
  username: string;

  @Prop()
  password: string;
}

export const UserSchema = SchemaFactory.createForClass(User);

export const hooks = [
  {
    name: User.name,
    useFactory: () => {
      const schema = UserSchema;
      schema.pre<User>('save', async function () {
        const pw = await Password.hash(this.password);
        this.password = pw;
      });
      return schema;
    },
  },
];
