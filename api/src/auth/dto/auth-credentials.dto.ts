import { ApiProperty } from '@nestjs/swagger';
import { IsString, Matches, MaxLength, MinLength } from 'class-validator';

export class AuthCredentialsDto {
  @ApiProperty({ example: 'appuser' })
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  readonly username: string;

  @ApiProperty({ example: '43X2$7#n@zRFf7$V*J!XZNpVsdK%dz' })
  @IsString()
  @MinLength(8)
  @MaxLength(32)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'Password is too weak.',
  })
  readonly password: string;
}
