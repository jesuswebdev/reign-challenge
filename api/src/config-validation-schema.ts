import * as Joi from 'joi';

export const configValidationSchema = Joi.object({
  JWT_SECRET: Joi.string().required(),
  DATABASE_HOST: Joi.string().default('mongodb'),
  DATABASE_PORT: Joi.number().default('27017'),
  SEED_ON_STARTUP: Joi.boolean().optional(),
  FETCH_ARTICLES_INTERVAL: Joi.number().integer().positive().default(60),
  PORT: Joi.number().default(3000),
  ARTICLES_PER_PAGE: Joi.number().integer().positive().min(1).default(5),
});
